import 'dart:io';

void main() {
  print("Install dart ? y/t : ");
  String inputText = stdin.readLineSync()!;

  inputText == "y"
      ? print("Anda akan menginstall aplikasi dart")
      : inputText == "t"
          ? print("Aborted")
          : print("Input tidak diketahui");
}
