import 'dart:io';

void main() {
  print("Silahkan input nama anda : ");
  String inputNama = stdin.readLineSync()!;
  print("Silahkan input peran anda : ");
  String inputPeran = stdin.readLineSync()!;

  inputNama == ""
      ? print("Nama harus diisi!")
      : inputPeran == "Penyihir"
          ? print(
              "Halo penyihir ${inputNama}, kamu dapat melihat siapa yang menjadi werewolf")
          : inputPeran == "Guard"
              ? print(
                  "Halo Guard ${inputNama}, kamu kan membantu melindungi temanmu dari serangan werewolf")
              : inputPeran == "Werewolf"
                  ? print(
                      "Halo Werewolf ${inputNama}, Kamu akan memakan mangsa setiap malam!")
                  : print(
                      "Halo ${inputNama}, Pilih peranmu untuk memulai game!");
}
