void main() {
  var pertama = 2;
  print("LOOPING PERTAMA");
  while (pertama <= 20) {
    print(pertama.toString() + " - I love coding");
    pertama++; // Mengubah nilai flag dengan menambahkan 1
  }

  var kedua = 20;
  print("LOOPING KEDUA");
  while (kedua >= 2) {
    print(kedua.toString() + " - I will become a mobile developer");
    kedua--;
  }
}
