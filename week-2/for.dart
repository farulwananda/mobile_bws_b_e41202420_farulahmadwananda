void main() {
  var x = 20;
  for (var angka = 1; angka <= x; angka++) {
    angka % 3 == 0 && angka % 2 == 1
        ? print("${angka} - I love coding")
        : angka % 2 == 1
            ? print("${angka} - Santai")
            : print("${angka} - Berkualitas");
  }
}
