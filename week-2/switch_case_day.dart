import 'dart:io';

void main() {
  print("Silahkan pilih hari : ");
  String inputHari = stdin.readLineSync()!;

  inputHari == "Senin"
      ? print(
          "Segala sesuatu memiliki kesudahan, yang sudah berakhir biarlah berlalu dan yakinlah semua akan baik-baik saja.")
      : inputHari == "Selasa"
          ? print(
              "Setiap detik sangatlah berharga karena waktu mengetahui banyak hal, termasuk rahasia hati.")
          : inputHari == "Rabu"
              ? print(
                  "Jika kamu tak menemukan buku yang kamu cari di rak, maka tulislah sendiri.")
              : inputHari == "Kamis"
                  ? print(
                      "Jika hatimu banyak merasakan sakit, maka belajarlah dari rasa sakit itu untuk tidak memberikan rasa sakit pada orang lain.")
                  : inputHari == "Jumat"
                      ? print("Hidup tak selamanya tentang pacar.")
                      : inputHari == "Sabtu"
                          ? print(
                              "Rumah bukan hanya sebuah tempat, tetapi itu adalah perasaan.")
                          : print("Silahkan input hari dengan benar!");
}
