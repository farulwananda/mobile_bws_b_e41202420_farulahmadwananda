import 'dart:convert';
import 'package:http/http.dart' show Client, Response;
import 'package:http/http.dart' as http;
import 'package:flutter_modul_moviedb/model/popular_movies.dart';

class ApiProvider {
  String apiKey = '073bd5e7b744a4ecba43980da74fe147';
  String baseUrl = 'https://api.themoviedb.org/3/movie/550';

  Client client = Client();

  Future<PopularMovies> getPopularMovies() async {
    //  String url = '$baseUrl/movie/popular?api_key=$apiKey';
    //  print(url);
    Response response =
        // await client.get('$baseUrl?api_key=$apiKey');
        await http.get(Uri.parse(baseUrl));
    // await client.get('$baseUrl/movie/popular?api_key=$apiKey');
    if (response.statusCode == 200) {
      return PopularMovies.fromJson(jsonDecode(response.body));
    } else {
      throw Exception(response.statusCode);
    }
  }
}
