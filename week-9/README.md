# Workshop Mobile Application Framework

## Description :

Learn how to implement Model, View, Controller (MVC) in Flutter

## Run project :

```sh
  flutter run
```
