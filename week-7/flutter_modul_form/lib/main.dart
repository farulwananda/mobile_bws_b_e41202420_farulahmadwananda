import 'package:flutter/material.dart';
import 'package:flutter_modul_form/FormPageKedua.dart';
import 'package:flutter_modul_form/FormPagePertama.dart';

void main() {
  runApp(MainPage());
}

class MainPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Home(),
    );
  }
}
