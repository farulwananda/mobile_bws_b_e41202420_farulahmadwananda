# Workshop Mobile Application Framework

## Description :

Learn how to make Create, Read, Update, Delete (CRUD) in flutter and connect it to REST Api

## Run project :

```sh
  flutter run
```
