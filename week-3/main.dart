import 'employee.dart';

void main(List<String> args) {
  Employee identitas1 = Employee.id('E41202420');
  Employee identitas2 = Employee.name('Farul Ahmad Wananda');
  Employee identitas3 = Employee.departement('Teknik Informatika');

  print(identitas1.id);
  print(identitas2.name);
  print(identitas3.departement);
}
